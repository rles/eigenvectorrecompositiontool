# EigenvectorRecompositionTool

Fill in theory 

## Setup

For running the base tool only a root installation is needed.

For running BJet recomposition analysis base needs to be setup as the BTaggingEigenVectorRecompositionTool need to be available:
https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency/Root/BTaggingEigenVectorRecompositionTool.cxx

Can setup both with the script:
```
./setup.sh
```

## Running

The basic operation of the tool is very simple
```
./run_modify.py <inputWorkspace> <additionalOptions>
```

You can tell the tool whether to do the JES or BTag uncertainity recomposition (or both) by the addition of the flags:
```
./run_modify.py <inputWorkspace> --doRecompJES
./run_modify.py <inputWorkspace> --doRecompBTag
```

The tool will identify parameters in the original workspace as being JES/Btag uncertainity by a search regex, the default being "jes/EFF\_Eigen". These can also be sepcified in the command line with:
```
./run_modify.py <inputWorkspace> --regexBTag/regexJES <regex>
```
