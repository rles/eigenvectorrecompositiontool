#!/usr/bin/env python
import ROOT
import argparse
import os,sys

#Need to set global so garbage collector doesn't remove these c++ objects after setup in function scope
evr_tool_core=None
evr_tool=None

def get_parser():
    _parser = argparse.ArgumentParser()
    _parser.add_argument('file')
    _parser.add_argument('--ws-name', default='combined')
    _parser.add_argument('--model-config', default='ModelConfig')
    _parser.add_argument('--outdir', default='./')
    _parser.add_argument('--debug', default=False, action='store_true')

    _parser.add_argument('--doRecompBTag', default=False, action='store_true')
    _parser.add_argument('--regexBTag', default="EFF_Eigen_B")
    _parser.add_argument('--ScaleFactorFileName', default='xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2019-07-30_v1.root')
    _parser.add_argument('--TaggerName', default='MV2c10')
    _parser.add_argument('--OperatingPoint', default='Continuous')
    _parser.add_argument('--JetAuthor', default='AntiKt4EMTopoJets')
    _parser.add_argument('--MinPt', default=20)

    _parser.add_argument('--doRecompJES', default=False, action='store_true')
    _parser.add_argument('--regexJES', default="jes")

    return _parser

def get_nps_matching(mc, expr, verbose=False):
    """
    Build list of all nuisance parameters in the WS and
    return the ones containing expr
    """
    _nps_of_interests = []
    for _np in mc.GetNuisanceParameters():
        if verbose:
            print(_np.GetName())
        if expr in _np.GetName():
            _nps_of_interests.append(_np.GetName())
    return _nps_of_interests

def make_np_linear_expression(eigen_np, orig_names, orig_coeffs):
    _expr = 'expr::{}_parameterized(\"'.format(eigen_np)
    for i, _coeff in enumerate(orig_coeffs):
        if i == 0:
            _expr += '{}*@{}'.format(_coeff, i)
        else:
            if _coeff < 0:
                _expr += '{}*@{}'.format(_coeff, i)
            else:
                _expr += '+{}*@{}'.format(_coeff, i)
    _expr += '\"'
    for _np in orig_names:
        _expr += ', {}'.format(_np)
    _expr += ')'
    return _expr

def get_btag_new_names():
  names=evr_tool.getListOfOriginalNuisanceParameters('B')
  for i in range(len(names)): names[i]="alpha_"+names[i]+"_EVR"
  return names

def get_btag_coefficients(index):
  return evr_tool.getCoefficients('B', index)

def get_jes_new_names():
  infile = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetUncertainties/CalibArea-08/rel21/Summer2019//R4_AllNuisanceParameters_AllJERNP.config"
  f = open(infile, "r")

  new_np_names=[]
  for line in f:
    if "JESGroup" in line: break
    if not "JESComponent" in line: continue
    if not "Name" in line: continue
    number=line.split(".")[1]
    name=line.split(" ")[-1]
    new_np_names.append("alpha_"+name[:-1]+"_EVR")

  return new_np_names

def get_jes_coefficients(index):
  name="ProjectionCoeff_back_EffectiveNP_"+str(index)+"_AntiKt4EMTopo";
  jetCorrelFile = "./GloballyReducedNPs_AntiKt4EMTopo.root";

  coeffs=[]
  f = ROOT.TFile(jetCorrelFile)
  h_coeff=f.Get(name)
  for i in range(1,h_coeff.GetNbinsX()+1): 
    coeffs.append(h_coeff.GetBinContent(i))
  return coeffs

def setup_btag_tool(sfFileName,taggerName,operatingPoint,jetAuthor,minPt):

  import ROOT
  ROOT.xAOD.Init().ignore()

  ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.NumIntegration) 
  ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Fitting) 
  ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Minimization) 
  ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.InputArguments) 
  ROOT.RooMsgService.instance().getStream(1).removeTopic(ROOT.RooFit.Eval) 
  ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)

  # btag tool setup (need to have properties as args?)
  btag_eff_tool_core = ROOT.BTaggingEfficiencyTool('BTagEffTool')
  btag_eff_tool = ROOT.ToolHandle('IBTaggingEfficiencyTool')('BTaggingEfficiencyTool/BTagEffTool')
  btag_eff_tool.setProperty("ScaleFactorFileName",sfFileName).ignore()
  btag_eff_tool.setProperty("TaggerName", taggerName).ignore()
  btag_eff_tool.setProperty("OperatingPoint", operatingPoint).ignore()
  btag_eff_tool.setProperty("JetAuthor", jetAuthor).ignore()
  btag_eff_tool.setProperty("MinPt", minPt).ignore()
  btag_eff_tool.initialize().ignore()

  # eigen vector recomposition tool
  global evr_tool_core
  evr_tool_core = ROOT.BTaggingEigenVectorRecompositionTool('EVRTool')
  global evr_tool
  evr_tool = ROOT.ToolHandle('IBTaggingEigenVectorRecompositionTool')("BTaggingEigenVectorRecompositionTool/EVRTool")
  evr_tool.setProperty("BTaggingEfficiencyTool", btag_eff_tool).ignore()
  evr_tool.initialize().ignore()

if __name__ == '__main__':
    
    # arguments from parser
    parser = get_parser()
    args = parser.parse_args()
    outfile_name = os.path.join(args.outdir, os.path.basename(args.file).replace('.root', '_recomposed.root'))

    #Print setting
    if args.doRecompJES:
      print("Attempting JES recompositon")
      print("\tLooking for uncertainities with regex %s"%args.regexJES)
    if args.doRecompBTag:
      print("Attempting BTag recompositon")
      print("\tLooking for uncertainities with regex %s"%args.regexBTag)
      print("\tUsing ScaleFactorFile %s"%args.ScaleFactorFileName)
      print("\tUsing TaggerName %s"%args.TaggerName)
      print("\tUsing OperatingPoint %s"%args.OperatingPoint)
      print("\tUsing JetAuthor %s"%args.JetAuthor)
      print("\tUsing MinPt %s"%args.MinPt)
    if not args.doRecompJES and not args.doRecompBTag:
      print("No recomposition procedure chosen")
      print("Have either doRecompBTag and/or doRecompJES turned on")
      sys.exit()
       
    # Load workspace
    f = ROOT.TFile.Open(args.file)
    workspace = f.Get(args.ws_name)
    mc = workspace.obj(args.model_config)

    original_nps=[]
    new_nps=[]
    exprs_to_add = []

    #Do JES recompostiion
    if args.doRecompJES:
      original_jes_nps=get_nps_matching(mc, expr=args.regexJES)
      new_jes_nps=get_jes_new_names()
      
      original_nps.extend(original_jes_nps)
      new_nps.extend(new_jes_nps)
      
      for eigen_np in original_jes_nps:
        idx = int(filter(str.isdigit, eigen_np))
        coeffs = get_jes_coefficients(idx)
        exprs_to_add.append(make_np_linear_expression(eigen_np, new_jes_nps, coeffs))

    #Do btag recomposition
    if args.doRecompBTag:
      setup_btag_tool(args.ScaleFactorFileName,args.TaggerName,args.OperatingPoint,args.JetAuthor,args.MinPt)
      original_btag_nps=get_nps_matching(mc, expr=args.regexBTag)
      new_btag_nps=get_btag_new_names()

      original_nps.extend(original_btag_nps)
      new_nps.extend(new_btag_nps)

      for eigen_np in original_btag_nps:
        idx = int(filter(str.isdigit, eigen_np))
        coeffs = get_btag_coefficients(idx) 
        exprs_to_add.append(make_np_linear_expression(eigen_np, new_btag_nps, coeffs)) 

    if len(exprs_to_add)==0:
      print("No systematics found matching your search regex")
      sys.exit()
    
    from modifier import modify_workspace
    modify_workspace( workspace, new_nps, exprs_to_add, original_nps, outfile_name=outfile_name, debug=args.debug)
