import ROOT

def modify_workspace(
        old_ws, 
        nps_to_add, 
        exprs_to_add, 
        nps_to_remove, 
        constraints_to_remove=None, 
        globs_to_remove=None,
        model_config_name='ModelConfig',
        outfile_name='new.root',
        debug=False):

    ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)

    #Load defaults if needed
    if globs_to_remove == None:
        globs_to_remove = ['nom_' + np for np in nps_to_remove]

    if constraints_to_remove == None:
        constraints_to_remove = [np + 'Constraint' for np in nps_to_remove]

    # check that the nps, global observables and constraints we plan to remove actually are there in the input workspace
    check_input_setup(old_ws, nps_to_remove, globs_to_remove, constraints_to_remove, exprs_to_add)

    # retrieve model config
    old_model_config = old_ws.obj(model_config_name)
    ws_name = old_ws.GetName()
    old_ws.SetName('oldWS')

    # define new workspace and ModelConfig
    print ('modifier: --> creating workspace {}'.format(ws_name))
    new_ws = ROOT.RooWorkspace(ws_name)
    new_model_config = ROOT.RooStats.ModelConfig(model_config_name, new_ws)
    #new_model_config.SetWS(new_ws)
    
    # import data
    for _data in old_ws.allData():
        getattr(new_ws, 'import')(_data)

    # Get current global observables
    new_globs = ROOT.RooArgSet()
    print('modifier: --> copying global observables') 
    for glob in old_model_config.GetGlobalObservables():
        if glob.GetName() in globs_to_remove:
            print('\t removing global obs: %s'%glob.GetName())
        else:
            new_globs.add(glob)

    # Adding new global observables
    for np in nps_to_add:
      glob_name = 'nom_' + np
      if old_ws.var(glob_name):
          print('Variable {} is already in the workspace.'.format(glob_name))
      else:
          this_glob=new_ws.factory("%s[0]"%glob_name)
          new_globs.add(this_glob)
    new_model_config.SetGlobalObservables(new_globs)
    
    # Get current nuisance parameters (alphas and gammas)
    new_nps = ROOT.RooArgSet()
    print('modifier: --> copying nuisance parameters') 
    for np in old_model_config.GetNuisanceParameters():
        if np.GetName() in nps_to_remove:
            print('\t removing nuisance param: %s'%np.GetName())
        else:
            new_nps.add(np)
    
    # Adding new NPs
    for np in nps_to_add:
      if old_ws.var(np):
          print('Variable {} is already in the workspace.'.format(np))
      else:
          this_np=new_ws.factory("%s[0,-5,5]"%np)
          new_nps.add(this_np)
    new_model_config.SetNuisanceParameters(new_nps)

    # observables
    new_observables = ROOT.RooArgSet()
    print('modifier: --> copying observables') 
    for obs in old_model_config.GetObservables():
        new_observables.add(obs)
    new_model_config.SetObservables(new_observables)

    # Adding new gaussian constraints
    print('modifier: --> Adding new gaussian constraints')
    constraints_to_add = []
    for np in nps_to_add:
        constraint = 'RooGaussian::{0}Constraint({0}, nom_{0}, 1)'.format(np)
        print('\t {}'.format(constraint))
        new_ws.factory(constraint)
        constraints_to_add.append(np+"Constraint")

    # Add expressions
    print('modifier: --> Adding expressions')
    for expression in exprs_to_add:
        print('\t {}'.format(expression.split("(")[0]+"(...)"))
        new_ws.factory(expression)

    # --- pdf building
    print('modifier: --> Creating new PDF')
    old_pdf = old_model_config.GetPdf()
    new_pdf=get_pdf(new_ws, old_pdf, constraints_to_add, constraints_to_remove, nps_to_remove) #saves pdf to new_ws also
    #getattr(new_ws, 'import')( new_pdf)

    # model config setup and import to the workspace
    new_model_config.SetPdf(new_pdf)
    new_model_config.SetParametersOfInterest(old_model_config.GetParametersOfInterest())
    getattr(new_ws, 'import')(new_model_config)

    #FIXME do something about snapshots ?
    #new_ws.importClassCode()
    new_ws.writeToFile(outfile_name)

    print("modifier: --> Done")
    print("\t Recomposed workspace {}".format(outfile_name))

def check_input_setup(ws, nuisance_parameters, global_observables, gaussian_constraints, expressions):
    """
    """
    if len(nuisance_parameters) != len(global_observables):
        raise ValueError('len(nuisance_parameters) != len(global_observables) ({} != {})'.format( len(nuisance_parameters), len(global_observables)))

    if len(nuisance_parameters) != len(gaussian_constraints):
        raise ValueError('len(nuisance_parameters) != len(gaussian_constraints) ({} != {})'.format( len(nuisance_parameters), len(gaussian_constraints)))

    if len(nuisance_parameters) != len(expressions):
        raise ValueError('not the same number of expressions and nps to remove ({} != {})'.format( len(exprs_to_add), len(nps_to_remove)))

    for np_name in nuisance_parameters:
        np = ws.var(np_name)
        try: np.GetName()
        except: raise NameError('{} NP constraint not in workspace!'.format(np_name))

    for glob_name in global_observables:
        glob = ws.var(glob_name)
        try: glob.GetName()
        except: raise NameError('{} global observable not in workspace!'.format(glob_name))

    for constraint in gaussian_constraints:
        pdf = ws.pdf(constraint)
        try: pdf.GetName()
        except: raise NameError('{} constraint not in workspace!'.format(constraint))

def get_pdf(ws, pdf, additional_pdfs, remove_pdfs, remove_nps):
  pdfs=ROOT.RooArgList()
  pdflist=[] #to keep from garbage collector

  #Loop over categories
  m_cat = pdf.indexCat()
  for index in range(m_cat.numTypes()):
    m_cat.setBin(index)

    pdfi = pdf.getPdf(m_cat.getLabel())
    baseComponents=ROOT.RooArgList()
    if pdfi.InheritsFrom("RooProdPdf"):
      for thispdf in get_base_pdfs(pdfi):
        if thispdf.GetName() in remove_pdfs: continue
        baseComponents.add(thispdf)

      for name in additional_pdfs:
        if thispdf.GetName() in remove_pdfs: continue
        baseComponents.add(ws.pdf(name))

    else:  #FIXME how would this happen?
      if pdfi.GetName() in remove_pdfs: continue
      baseComponents.add(pdfi)

    #Make prod pdf for this region
    new_channel_pdf = ROOT.RooProdPdf(pdfi.GetName()+"_new", pdfi.GetName()+"_new", baseComponents)
    pdfs.add(new_channel_pdf)
    pdflist.append(new_channel_pdf)

  #Make the new RooSimulateous, aka top pdf
  newPdf = ROOT.RooSimultaneous(pdf.GetName(), pdf.GetTitle(), pdfs, m_cat)
  replace_old=[]
  replace_new=[]
  for np in remove_nps:
    replace_old.append(np)
    replace_new.append(np+"_parameterized")
  getattr(ws, 'import')( newPdf, ROOT.RooFit.RenameVariable(",".join(replace_old), ",".join(replace_new)), ROOT.RooFit.RecycleConflictNodes())

  return newPdf

def get_base_pdfs(pdf):
  argset=ROOT.RooArgSet()
  pdfList = pdf.pdfList()
  if pdfList.getSize()==1:
    argset.add(pdfList)
  else:
    for arg in  pdfList:
      if arg.ClassName() != "RooProdPdf":
        argset.add(arg)
      else:
        get_base_pdfs(arg, argset)
  return argset
